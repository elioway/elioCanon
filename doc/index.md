> In 1981 (or whatever) I watched Blake ask a glass box to "plot a safe course to the Andromeda system". So why, in 2023, does the computer need _my_ description of an `Address`, or for _me_ to add a `deliveryDate` field to a `Delivery` object and explain how it needs to be a `Date` type? **Tim Bushell**

# all things shall be your prey

<aside>
  <dl>
  <dd>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
      Be this, or aught</dd>
  <dd>Than this more secret, now designed, I haste</dd>
  <dd>To know; and, this once known, shall soon return,</dd>
  <dd>And bring ye to the place where thou and Death</dd>
  <dd>Shall dwell at ease, and up and down unseen</dd>
  <dd>Wing silently the buxom air, embalmed</dd>
  <dd>With odours. There ye shall be fed and filled</dd>
  <dd>Immeasurably; all things shall be your prey.</dd>
</dl>
</aside>

No more modelling. No more tables. No more SQL.

**eliothing** provides all the schema and data models you'll need, and describes a _design_pattern_.

Each record in the database is known as a "thing".

**eliothing** is a source for auto-generating MVC code, classes, models, views, and controllers based on <https://schema.org> _schemaTypes_, for existing frameworks, **the elioWay**.

[eliothing Dogma](/eliothing/dogma.html)

## Thing Builders

**thing** is the most basic thingBuilder. It builds Things templated like `T[T]`, where `T` is a class of `Thing`, listing other things of class `Thing`. **thing** will convert those as coordinates to <https://schema.org> `jsonld` valid, meaningfully named class types.

## `Thing`

- any Schema.org datatypes which inherit from `Thing`.
- a class name from another schema framework
- an engaged elioApp, which in our _design_pattern_, makes it a custom subclass of `Thing` :

## Building

The `npm run thing -- Thing --schema` command will return:

```
{
  "Thing": {
    "potentialAction": "Text",
    "identifier": "Text",
    "sameAs": "URL",
    "url": "URL",
    "image": "URL",
    "alternateName": "Text",
    "name":"Text",
    "description": "Text",
    "mainEntityOfPage": "URL",
    "disambiguatingDescription": "Text",
    "subjectOf": "Text",
    "additionalType": "URL",
  }
}
```

You could easily reuse this to autogenerate MVC classes for a range of frameworks like **MongooseJs** and **Django**.

## Begotten Things

<section>
  <figure>
  <a href="/eliothing/generator-thing/artwork/splash.jpg" target="_splash"><img src="/eliothing/generator-thing/artwork/splash.jpg" target="_splash"></a>
  <h3>CLI</h3>
  <p>In the grand scheme of Things, the <strong>elioWay</strong>.</p>
  <p><a href="/eliothing/thing/"><button><img src="/eliothing/thing/favicoff.png"><br>thing</button></a></p>
</figure>
  <figure>
  <a href="/eliothing/liar-thing/artwork/splash.jpg" target="_splash"><img src="/eliothing/liar-thing/artwork/splash.jpg" target="_splash"></a>
  <h3>Fake</h3>
  <p>Generating fake and random data for <a href="https://schema.org">schema.org</a> Things, the <strong>elioWay</strong>.</p>
  <p><a href="/eliothing/liar-thing/"><button><img src="/eliothing/liar-thing/favicoff.png"><br>liar-thing</button></a></p>
</figure>
  <figure>
  <a href="/eliothing/mongoose-thing/artwork/splash.jpg" target="_splash"><img src="/eliothing/mongoose-thing/artwork/splash.jpg" target="_splash"></a>
  <h3>MongooseJs</h3>
  <p>Converts  <a href="https://schema.org">schema.org</a> into <strong>MongooseJs</strong> Model objects of Things which you can instantiate and save to a MongoDb, the <strong>elioWay</strong>.</p>
  <p><a href="/eliothing/mongoose-thing/"><button><img src="/eliothing/mongoose-thing/favicoff.png"><br>mongoose-thing</button></a></p>
</figure>
</section>

## Related

<article>
  <a href="/eliobones/">
  <img src="/eliobones/favicoff.png">
  <div>
  <h4>eliobones</h4>
  <p>Cruddylicious endpoints for your <strong>eliothings</strong>, the <strong>elioWay</strong>.
</p>
</div>
</a>
</article>

<article>
  <a href="/elioflesh/">
  <img src="/elioflesh/favicoff.png">
  <div>
  <h4>elioflesh</h4>
  <p>Instantly sexy, client apps built from <strong>eliothings</strong> and <strong>eliobones</strong>, the <strong>elioWay</strong>.
</p>
</div>
</a>
</article>
